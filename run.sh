#!/bin/bash

set -e

MAIN_CLASS="eu.smile.elasticsuite.ml.EventProcessorApp"

if [ -z $SPARK_MASTER ]; then
    SPARK_MASTER="local[8]"
fi;

if [ -z $SPARK_HOME ]; then
    SPARK_HOME="/usr/local/spark"
fi;

if [ $1 ]; then
    ARGS="$ARGS --date $1"
fi;

if [ $ES_HOST ]; then
    ARGS="$ARGS --esHost $ES_HOST"
fi;

if [ $RETENTION_DELAY ]; then
    ARGS="$ARGS --retentionDelay $RETENTION_DELAY"
fi;

if [ -z $OUTPUT_DIR ]; then
    OUTPUT_DIR="output"
fi;

if [ -z $TMP_DIR ]; then
    TMP_DIR="tmp"
fi;

if [ -d $TMP_DIR ]; then
    rm -rf $TMP_DIR
fi

if [ -d $OUTPUT_DIR ]; then
    rm -rf $OUTPUT_DIR
fi;

$SPARK_HOME/bin/spark-submit --class $MAIN_CLASS --master $SPARK_MASTER event-processor.jar $ARGS --outputDir $TMP_DIR

mkdir -p $OUTPUT_DIR
mv $TMP_DIR/queryWeight/*/*.csv $OUTPUT_DIR/queryWeight.csv
mv $TMP_DIR/correlations/*/*.csv $OUTPUT_DIR/correlations.csv
rm -rf $TMP_DIR
