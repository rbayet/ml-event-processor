#!/bin/bash

if [ -d build ]; then
    rm -rf build;
fi;

mkdir -p build/event-processor
sbt package
cp -rf target/scala-2.11/event-processor_2.11-1.0.jar build/event-processor/event-processor.jar
cp run.sh build/event-processor/
cd build/event-processor && tar zcf ../event-processor.tar.gz . && cd -
