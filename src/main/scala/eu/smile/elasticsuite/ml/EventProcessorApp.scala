package eu.smile.elasticsuite.ml

import eu.smile.elasticsuite.ml.config.EventProcessorConfig
import eu.smile.elasticsuite.ml.event.EventDatasource
import eu.smile.elasticsuite.ml.algorithm.Processor
import eu.smile.elasticsuite.ml.algorithm.queryweight.QueryWeightProcessor
import eu.smile.elasticsuite.ml.algorithm.correlations.CorrelationProcessor

object EventProcessorApp {
  def main(args: Array[String]) {
    val config = EventProcessorConfig(args)
    val outputPrefix = config.outputDir + "/" + config.startDate

    val processors: Map[String, Processor] = Map(
        "queryWeight" -> new QueryWeightProcessor(),
        "correlations" -> new CorrelationProcessor()
    )
    var datasource = EventDatasource.createDataFrame();

    for ((processorName, processor) <- processors) {
        val output = processor.process(datasource)

        println("Report (" + processorName + ") ==============")
        for ((metricName, metricValue) <- output.metrics) {
            println("   " + metricName + " : " + metricValue)
        }

        for ((dfName, df) <- output.datasets) {
            val outputFileName = outputPrefix + "/" + processorName + "/" + dfName
            df.coalesce(1)
              .write.format("com.databricks.spark.csv")
              .option("header", "true")
              .save(outputFileName)
        }
    }
  }
}
