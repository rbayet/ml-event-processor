package eu.smile.elasticsuite.ml.algorithm.queryweight

import eu.smile.elasticsuite.ml.algorithm.{Processor,Output}

import org.apache.spark.ml.{Pipeline,PipelineModel}
import org.apache.spark.ml.regression.RandomForestRegressor
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.linalg.{Vectors,Vector}

import org.apache.spark.sql.{Dataset,Row}
import org.apache.spark.sql.functions._

class QueryWeightProcessor() extends Processor {

    private val validateEntry = udf[Boolean,Vector](_.numNonzeros > 3)

    def process(source: Dataset[Row]) : Output = {
        val queryHistoryDF                = extractQueriesHistory(source)
        val Array(trainingData, testData) = getTrainingData(queryHistoryDF).randomSplit(Array(0.9, 0.1))
        val model                         = trainModel(trainingData)

        val trainingDataSize = trainingData.count()
        val evaluation       = evaluateModel(model, testData)
        val predictions      = executeModel(model, queryHistoryDF)

        new Output(
            Map("training_data_size" -> queryHistoryDF.count(), "rmse" -> evaluation),
            Map("predictions" -> predictions)
        )
    }

    private def getTrainingData(data: Dataset[Row]) : Dataset[Row] = {
        import data.sparkSession.implicits._

        data.rdd.map(row => {
            val history = row.getSeq(1).asInstanceOf[Seq[Double]].toArray
            (row.getString(0), Vectors.dense(history.slice(0, history.length-1)), history.last)
        })
        .toDF()
        .select($"_1".as("query"), $"_2".as("history"), $"_3".as("weight"))
        .filter(validateEntry('history))
    }

    private def trainModel(data: Dataset[Row]) : PipelineModel = {
        val rf = new RandomForestRegressor().setLabelCol("weight").setFeaturesCol("history")
        val pipeline = new Pipeline().setStages(Array(rf))
        pipeline.fit(data)
    }

    private def evaluateModel(model: PipelineModel, data: Dataset[Row]) : Double = {
        val evaluations = model.transform(data)
        val evaluator = new RegressionEvaluator().setLabelCol("weight").setPredictionCol("prediction").setMetricName("rmse")
        evaluator.evaluate(evaluations)
    }

    private def executeModel(model: PipelineModel, data: Dataset[Row]) : Dataset[Row] = {
        import data.sparkSession.implicits._

        val runData = data.rdd.map(row => {
            val history = row.getSeq(1).asInstanceOf[Seq[Double]].toArray
            (row.getString(0), Vectors.dense(history.slice(1, history.length)))
        })
        .toDF()
        .select($"_1".as("query"), $"_2".as("history"))
        .filter(validateEntry('history))

        model.transform(runData)
             .select($"query", $"prediction".as("weight"))
             .orderBy($"weight".desc)
    }

    private def extractQueriesHistory(source: Dataset[Row]) : Dataset[Row] = {
        import source.sparkSession.implicits._

        source.filter($"session.uid".isNotNull && $"page.search.query".isNotNull)
          .select($"date", $"session.uid".as("session_id"), $"page.search.query".as("query"))
          .groupBy($"date", $"query")
          .agg(countDistinct($"session_id").as("uniqueSessions"))
          .groupBy($"query")
          .pivot("date")
          .agg(first($"uniqueSessions").cast("double"))
          .rdd.map(row => (row.getString(0), row.toSeq.slice(1, row.length).asInstanceOf[Seq[Double]]))
          .toDF()
          .select($"_1".as("query"), $"_2".as("history"))
          .cache()
    }
}
