package eu.smile.elasticsuite.ml.algorithm

import org.apache.spark.sql.{Dataset,Row}

trait Processor {
  def process(source: Dataset[Row]) : Output
}
